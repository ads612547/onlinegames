package com.example.onlinegame;


import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class firstpage extends AppCompatActivity {
    private int player1Count = 0;
    private int aiCount = 0;
    private boolean playerTurn = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firstpage);

        TextView player1TextView = findViewById(R.id.textView);
        TextView aiTextView = findViewById(R.id.textView2);

        Button resetButton = findViewById(R.id.button10);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetGame();
            }
        });

        // Set initial scores
        player1TextView.setText("Player1 : " + player1Count);
        aiTextView.setText("AI : " + aiCount);

        // Set click listeners for game buttons
        setButtonClickListeners();
    }

    private void setButtonClickListeners() {
        int[] buttonIds = {R.id.button, R.id.button2, R.id.button3, R.id.button4, R.id.button5,
                R.id.button6, R.id.button7, R.id.button8, R.id.button9};

        for (int id : buttonIds) {
            Button button = findViewById(id);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onButtonClick(button);
                }
            });
        }
    }

    private void onButtonClick(Button button) {
        if (playerTurn) {
            // Handle player move
            button.setText("X");
            // Add logic for player move

            // Check for a winner
            if (checkForWinner()) {
                // Player wins
                player1Count++;
                updateScores();
                resetGame();
            } else {
                // Switch turn to AI
                playerTurn = false;

                // Add delay before AI move (for a better user experience)
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        makeAiMove();
                        // Switch turn back to player for the next round
                        playerTurn = true;

                        // Check for a winner after AI move
                        if (checkForWinner()) {
                            // AI wins
                            aiCount++;
                            updateScores();
                            resetGame();
                        }
                    }
                }, 500);
            }
        }
    }

    private void makeAiMove() {
        // Implement your AI logic here
        // This is a simple example - you need to replace it with your AI algorithm

        int[] buttonIds = {R.id.button, R.id.button2, R.id.button3, R.id.button4, R.id.button5,
                R.id.button6, R.id.button7, R.id.button8, R.id.button9};

        // Find an available button for the AI move (for simplicity, it selects the first available button)
        for (int id : buttonIds) {
            Button button = findViewById(id);
            if (button.getText().toString().isEmpty()) {
                button.setText("O");
                // Add logic for AI move
                break;
            }
        }
    }

    private boolean checkForWinner() {
        // Implement your logic to check for a winner
        // This is a simple example - you need to replace it with your winning conditions
        // For example, you can check rows, columns, and diagonals for three in a row

        // Rows
        for (int i = 0; i < 3; i++) {
            if (checkRow(i)) {
                return true;
            }
        }

        // Columns
        for (int i = 0; i < 3; i++) {
            if (checkColumn(i)) {
                return true;
            }
        }

        // Diagonals
        if (checkDiagonal() || checkReverseDiagonal()) {
            return true;
        }

        return false;
    }

    private boolean checkRow(int row) {
        return checkCell(row, 0) && checkCell(row, 1) && checkCell(row, 2);
    }

    private boolean checkColumn(int column) {
        return checkCell(0, column) && checkCell(1, column) && checkCell(2, column);
    }

    private boolean checkDiagonal() {
        return checkCell(0, 0) && checkCell(1, 1) && checkCell(2, 2);
    }

    private boolean checkReverseDiagonal() {
        return checkCell(0, 2) && checkCell(1, 1) && checkCell(2, 0);
    }

    private boolean checkCell(int row, int column) {
        Button button = findViewById(getButtonId(row, column));
        return button.getText().toString().equals("X");
    }

    private int getButtonId(int row, int column) {
        int[][] buttonIds = {
                {R.id.button, R.id.button2, R.id.button3},
                {R.id.button4, R.id.button5, R.id.button6},
                {R.id.button7, R.id.button8, R.id.button9}
        };
        return buttonIds[row][column];
    }

    private void resetGame() {
        // Clear the buttons on the board
        int[] buttonIds = {R.id.button, R.id.button2, R.id.button3, R.id.button4, R.id.button5,
                R.id.button6, R.id.button7, R.id.button8, R.id.button9};

        for (int id : buttonIds) {
            Button button = findViewById(id);
            button.setText("");
        }
    }

    private void updateScores() {
        TextView player1TextView = findViewById(R.id.textView);
        TextView aiTextView = findViewById(R.id.textView2);

        player1TextView.setText("Player1 : " + player1Count);
        aiTextView.setText("AI : " + aiCount);
    }
}
