package com.example.onlinegame;



import static java.lang.System.exit;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class offline extends AppCompatActivity {

    private boolean playerTurn = true;

    private int player1Count = 0;
    private int player2Count = 0;

    private ArrayList<Integer> player1 = new ArrayList<>();
    private ArrayList<Integer> player2 = new ArrayList<>();
    private ArrayList<Integer> emptyCells = new ArrayList<>();
    private int activeUser = 1;
    private boolean singleUser = false; // Add this line to declare 'singleUser' variable

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline);

        Button button10 = findViewById(R.id.button10);
        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });
    }

    public void clickfun(View view) {
        if (playerTurn) {
            Button buttonSelected = (Button) view;
            int cellID = 0;

            switch (buttonSelected.getId()) {
                case R.id.button:
                    cellID = 1;
                    break;
                case R.id.button2:
                    cellID = 2;
                    break;
                case R.id.button3:
                    cellID = 3;
                    break;
                case R.id.button4:
                    cellID = 4;
                    break;
                case R.id.button5:
                    cellID = 5;
                    break;
                case R.id.button6:
                    cellID = 6;
                    break;
                case R.id.button7:
                    cellID = 7;
                    break;
                case R.id.button8:
                    cellID = 8;
                    break;
                case R.id.button9:
                    cellID = 9;
                    break;
            }

            playerTurn = false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    playerTurn = true;
                }
            }, 600);

            playnow(buttonSelected, cellID);
        }
    }

    public void playnow(Button buttonSelected, int currCell) {
        MediaPlayer audio = MediaPlayer.create(this, R.raw.poutch);

        if (activeUser == 1) {
            buttonSelected.setText("X");
            buttonSelected.setTextColor(Color.parseColor("#EC0C0C"));
            player1.add(currCell);
            emptyCells.add(currCell);
            audio.start();
            buttonSelected.setEnabled(false);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    audio.release();
                }
            }, 200);

            int checkWinner = checkwinner();
            if (checkWinner == 1) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        reset();
                    }
                }, 2000);
            } else if (singleUser) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        robot();
                    }
                }, 500);
            } else {
                activeUser = 2;
            }

        } else {
            buttonSelected.setText("O");
            audio.start();
            buttonSelected.setTextColor(Color.parseColor("#D22BB804"));
            activeUser = 1;
            player2.add(currCell);
            emptyCells.add(currCell);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    audio.release();
                }
            }, 200);

            buttonSelected.setEnabled(false);
            int checkWinner = checkwinner();
            if (checkWinner == 1) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        reset();
                    }
                }, 4000);
            }
        }
    }

    public int checkwinner() {
        MediaPlayer audio = MediaPlayer.create(this, R.raw.success);

        if ((player1.contains(1) && player1.contains(2) && player1.contains(3)) ||
                (player1.contains(1) && player1.contains(4) && player1.contains(7)) ||
                (player1.contains(3) && player1.contains(6) && player1.contains(9)) ||
                (player1.contains(7) && player1.contains(8) && player1.contains(9)) ||
                (player1.contains(4) && player1.contains(5) && player1.contains(6)) ||
                (player1.contains(1) && player1.contains(5) && player1.contains(9)) ||
                (player1.contains(3) && player1.contains(5) && player1.contains(7)) ||
                (player1.contains(2) && player1.contains(5) && player1.contains(8))) {
            player1Count += 1;
            buttonDisable();
            audio.start();
            disableReset();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    audio.release();
                }
            }, 4000);

            AlertDialog.Builder build = new AlertDialog.Builder(this);
            build.setTitle("Game Over");
            build.setMessage("You have won the game.." + "\n\n" + "Do you want to play again");
            build.setPositiveButton("Ok", (dialog, which) -> {
                reset();
                audio.release();
            });
            build.setNegativeButton("Exit", (dialog, which) -> {
                audio.release();
                exit(1);
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    build.show();
                }
            }, 2000);

            return 1;

        } else if ((player2.contains(1) && player2.contains(2) && player2.contains(3)) ||
                (player2.contains(1) && player2.contains(4) && player2.contains(7)) ||
                (player2.contains(3) && player2.contains(6) && player2.contains(9)) ||
                (player2.contains(7) && player2.contains(8) && player2.contains(9)) ||
                (player2.contains(4) && player2.contains(5) && player2.contains(6)) ||
                (player2.contains(1) && player2.contains(5) && player2.contains(9)) ||
                (player2.contains(3) && player2.contains(5) && player2.contains(7)) ||
                (player2.contains(2) && player2.contains(5) && player2.contains(8))) {
            player2Count += 1;
            audio.start();
            buttonDisable();
            disableReset();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    audio.release();
                }
            }, 4000);

            AlertDialog.Builder build = new AlertDialog.Builder(this);
            build.setTitle("Game Over");
            build.setMessage("Opponent have won the game" + "\n\n" + "Do you want to play again");
            build.setPositiveButton("Ok", (dialog, which) -> {
                reset();
                audio.release();
            });
            build.setNegativeButton("Exit", (dialog, which) -> {
                audio.release();
                exit(1);
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    build.show();
                }
            }, 2000);

            return 1;
        } else if (emptyCells.contains(1) && emptyCells.contains(2) && emptyCells.contains(3) &&
                emptyCells.contains(4) && emptyCells.contains(5) && emptyCells.contains(6) &&
                emptyCells.contains(7) && emptyCells.contains(8) && emptyCells.contains(9)) {

            AlertDialog.Builder build = new AlertDialog.Builder(this);
            build.setTitle("Game Draw");
            build.setMessage("Nobody Wins" + "\n\n" + "Do you want to play again");
            build.setPositiveButton("Ok", (dialog, which) -> reset());
            build.setNegativeButton("Exit", (dialog, which) -> exit(1));
            build.show();
            return 1;
        }
        return 0;
    }

    public void reset() {
        player1.clear();
        player2.clear();
        emptyCells.clear();
        activeUser = 1;

        for (int i = 1; i <= 9; i++) {
            Button buttonSelected = getButtonById(i);
            buttonSelected.setEnabled(true);
            buttonSelected.setText("");
        }

        // Change from Button to TextView
        TextView textView = findViewById(R.id.textView);
        TextView textView2 = findViewById(R.id.textView2);
        textView.setText("Player1 : " + player1Count);
        textView2.setText("Player2 : " + player2Count);

    }

    public void robot() {
        int rnd;
        do {
            rnd = (int) (1 + Math.random() * 9);
        } while (emptyCells.contains(rnd));

        Button buttonSelected = getButtonById(rnd);
        emptyCells.add(rnd);

        MediaPlayer audio = MediaPlayer.create(this, R.raw.poutch);
        audio.start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                audio.release();
            }
        }, 500);

        buttonSelected.setText("O");
        buttonSelected.setTextColor(Color.parseColor("#D22BB804"));
        player2.add(rnd);
        buttonSelected.setEnabled(false);

        int checkWinner = checkwinner();
        if (checkWinner == 1) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    reset();
                }
            }, 2000);
        }
    }

    public void buttonDisable() {
        for (int i = 1; i <= 9; i++) {
            Button buttonSelected = getButtonById(i);
            if (buttonSelected.isEnabled()) {
                buttonSelected.setEnabled(false);
            }
        }
    }

    public void disableReset() {
        Button button10 = findViewById(R.id.button10);
        button10.setEnabled(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                button10.setEnabled(true);
            }
        }, 2200);
    }

    private Button getButtonById(int id) {
        switch (id) {
            case 1:
                return findViewById(R.id.button);
            case 2:
                return findViewById(R.id.button2);
            case 3:
                return findViewById(R.id.button3);
            case 4:
                return findViewById(R.id.button4);
            case 5:
                return findViewById(R.id.button5);
            case 6:
                return findViewById(R.id.button6);
            case 7:
                return findViewById(R.id.button7);
            case 8:
                return findViewById(R.id.button8);
            case 9:
                return findViewById(R.id.button9);
            default:
                return findViewById(R.id.button);
        }
    }
}
