package com.example.onlinegame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class secondpage extends AppCompatActivity {
    public static  boolean Online = true;
    public static  boolean singleUser = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondpage);

        Button offline = findViewById(R.id.offline);
        Button online = findViewById(R.id.online);

        offline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(secondpage.this, offline.class);
                startActivity(intent);
                singleUser = false;
                Online = false;
            }
        });
        online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(secondpage.this,online.class);
                startActivity(intent);
                singleUser = true;
                Online = true;
            }
        });
    }
}